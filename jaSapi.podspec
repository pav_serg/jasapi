Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.3'
s.name = "jaSapi"
s.summary = "jaSapi is a part of Jaja as separated network request manager"
s.requires_arc = true

# 2
s.version = "0.0.1"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Pavlo Dumyak" => "pavlo.dumyak@kindgeek.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://bitbucket.org/pav_serg/jasapi/src/master"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://pav_serg@bitbucket.org/pav_serg/jasapi.git",
:tag => "#{s.version}" }

# 7
s.framework = "UIKit"

s.static_framework = true

s.dependency 'Crashlytics'

# 8
s.source_files = "jaSapi/Core/*.{swift}"

# 10
s.swift_version = "5.0"

end
