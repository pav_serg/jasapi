//
//  ServiceRequestHandler.swift
//  Dispatcher
//
//  Created by Nick Savula on 11/15/16.
//  Copyright © 2016 Nick Savula. All rights reserved.
//

import Foundation
import Crashlytics

class ServiceRequestHandler: RequestHandler {
    // MARK: globals
    // enclosing defaults structure
    private struct serviceDefaults {
        private(set) var logNetwork: Bool?
        
        init() {
            let plistPath = Bundle.main.path(forResource: "Dispatcher-info", ofType: "plist")
            if let plist = NSDictionary(contentsOfFile: plistPath!) as? [String: Any] {
                #if DEBUG
                    logNetwork = plist["LOG_NETWORK"] as? Bool
                #endif
            }
        }
    }
    
    private static let defaults = serviceDefaults()
    
    // MARK: processing
    override func processRequest(request: Request, error: Error?) {
        var error = error
        
        let serviceRequest = request.serviceURLRequest()
        
        #if DEBUG
        if let logNetwork = ServiceRequestHandler.defaults.logNetwork, logNetwork == true {
            NetworkLogger.logNetworkRequest(request: serviceRequest, requestName: request.typeName)
        }
        #endif
        
        let methodStart = Date()
        let semaphore = DispatchSemaphore.init(value: 0)
        let task = URLSession.shared.dataTask(with: serviceRequest) {
            data, response, networkError in
            
            #if DEBUG
            if let logNetwork = ServiceRequestHandler.defaults.logNetwork, logNetwork == true {
                NetworkLogger.logNetworkResponse(response: response, error: networkError, data: data, requestName: request.typeName)
            }
            #endif
            
            if request.canceled {
                print("request cancelled: \(request)")
            } else if let networkError = networkError {
                error = networkError
                Crashlytics.sharedInstance().recordError(networkError)
            } else {
                do {
                    try request.response.parseResponse(response: response, data: data)
                } catch let parsingError {
                    error = parsingError
                }
            }
            
            let methodFinish = Date()
            let executionTime = methodFinish.timeIntervalSince(methodStart)
            
            if executionTime > 1 { // more then one second
                Answers.logCustomEvent(withName: "Request Execution Time", customAttributes: ["Network Request": "\(serviceRequest.url?.absoluteString ?? "FATAL - invalid URL")", "execution time": "\(executionTime)", "request name": request.typeName])
            }
            
            if let logNetwork = ServiceRequestHandler.defaults.logNetwork, logNetwork == true {
                print("[NETWORK REQUEST] \(serviceRequest.url?.absoluteString ?? "FATAL - invalid URL") execution time: \(executionTime)")
            }
            semaphore.signal()
        }
        
        if request.cancelation == nil {
            request.cancelation = {
                task.cancel()
                semaphore.signal()
            }
        }
        
        task.resume()
        
        if semaphore.wait(timeout: DispatchTime.distantFuture) == .timedOut {
            // TODO: handle timeout (throw error)
        }
        
        super.processRequest(request: request, error: error)
    }
}
