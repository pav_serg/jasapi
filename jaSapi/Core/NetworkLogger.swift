//
//  NetworkLogger.swift
//  HyperJar
//
//  Created by Kindgeek on 7/16/18.
//  Copyright © 2018 KindGeek. All rights reserved.
//

import Foundation

class NetworkLogger {
    
   fileprivate static var skipPrintExtensionsContainer = true
    
    class func logNetworkRequest(request: URLRequest, requestName: String? = nil) {
        
        var headers = ""
        if let headerFields = request.allHTTPHeaderFields {
            for (key, value) in headerFields {
                headers.append("    \(key): \(value)\n")
            }
        }
        
        var body = "    <empty>"
        if let data = request.httpBody, data.count > 0 {
            if data.count > 2048 {
                body = String("    <\(data.count) bytes>")
            } else if let dataString = String(data: data, encoding: .utf8) {
                body = dataString
            }
        }
        
        if let url = request.url?.absoluteString, let method = request.httpMethod {
            print("""
                \n----- [NETWORK REQUEST] -----
                  URL: \(url)
                  REQUEST CLASS: \(requestName ?? "Empty")
                  METHOD: \(method)
                  HEADER FIELDS\n{\n\(headers)}
                  BODY\n\(body)
                ------------------------------\n
                """)
        }
    }
    
    class func logNetworkResponse(response: URLResponse?, error: Error?, data: Data?, requestName: String? = nil) {
        if let networkError = error {
            print("\n----- [NETWORK RESPONSE] -----\n  ERROR: \(networkError.localizedDescription)\n")
        } else if let networkResponse = response as? HTTPURLResponse  {
            var headers = ""
            for (key, value) in networkResponse.allHeaderFields {
                headers.append("    \(key): \(value)\n")
            }
            
            var body = "    <empty>"
            if let responseData = data, responseData.count > 0 {
                if let dataString = String(data: responseData, encoding: .utf8) {
                    
                    if skipPrintExtensionsContainer {
                        if let index = dataString.range(of: ",\"extensions")?.lowerBound {
                            let substring = dataString[..<index]
                            let string = String(substring + "}")
                            body = string
                        } else {
                            body = dataString
                        }
                    } else {
                        body = dataString
                    }
                    
                }
            }
                        
            if let url = networkResponse.url?.absoluteString {
                print("""
                \n----- [NETWORK RESPONSE] -----
                  URL: \(url)
                  REQUEST CLASS: \(requestName ?? NSStringFromClass(HTTPURLResponse.self))
                  STATUS CODE: \(networkResponse.statusCode)
                  HEADER FIELDS\n{\n\(headers)}
                  BODY\n\(body)
                ------------------------------\n
                """)
            }
        }
    }
}
